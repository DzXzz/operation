#include<stdio.h>
#include<stdlib.h>
#include <time.h>
#define N 30

int GCD(int k,int l) //求最大公约数
{
 		int  temp;          /*定义整型变量*/  
  		if(k<l)             /*通过比较求出两个数中的最大值和最小值*/  
 		 {   
     		temp=k;  
     		 k=l;  
     		 l=temp;  
   		 } /*设置中间变量进行两数交换*/  
   		while(l!=0)           /*通过循环求两数的余数，直到余数为0*/  
   		{  
     		 temp=k%l;  
      		k=l;              /*变量数值交换*/  
      		l=temp;  
   		 }  
  		return k;      
}

int LCM(int k, int l)
{
        int temp;  
  		temp=GCD(k,l);  /*调用自定义函数，求出最大公约数*/  
  		return  (k*l/temp);             //最大公约数*最小公倍数=两数乘积 求最小公倍数   
}


main()
{
    int a,b,c,j=0,i,n,num1,num2,num3,num4;
    int e,f,g,h,getanswer;//e为记录用户的题量，f为用户的答题答案 
    float rate;
    char op,op1;
    int num5,num6,num7,num8;//临时变量
    srand(time(NULL));
    do{
    printf("\n请输入数字类型(1.整数 2.真分数) -1为退出程序:     ");
    while(!(scanf("%d",&n)))
{
printf("\n 非法输入，请重新输入：");
fflush(stdin);
}
    
    switch(n)
    {
    case 1:
        {
            printf("用户请输入题量：");
            while(!(scanf("%d",&e)))
            {
                printf("\n 非法输入，请重新输入：");
                fflush(stdin);
            }
            for(i=0;i<e;i++)
            {
                a=rand()%100+1;
                b=rand()%100+1;
                c=rand()%4+1;
                printf("%d",a);
                switch(c)
                {
                case 1:printf("+");
                    op='+';
                    break;
                case 2:printf("-");
                        op='-';
                        break;
                case 3:printf("*");
                        op='*';
                        break;
                case 4:printf("/");
                        op='/';
                        break;
                }
                printf("%d=",b);
                while(!(scanf("%d",&f)))
                {
                    printf("\n 非法输入，请重新输入：");
                    fflush(stdin);
                }
                switch(op)
                {   
                case '+':   
                    getanswer=a+b;   
                    break;
                case '-':   
                    getanswer=a-b;
                    break;
                case '*':   
                    getanswer=a*b;
                    break;
                case '/':  
                    getanswer=a/b;   
                    break; 
                }
                if(f==getanswer)
                {
                    printf("\n做对了\n");
                    j++;
                }
                else
                {
                    printf("\n很遗憾，答错了...正确答案是%d \n",getanswer);
                    
                }
            }
            	 rate=1.0*j/e*100;
                 printf("用户总共答对了%d道题目！ 正确率为%.2f%%\n",j,rate);
            break;
        }
            break;
    case 2:
        {    printf("用户请输入题量：");
            while(!(scanf("%d",&e)))
            {
                printf("\n 非法输入，请重新输入：");
                fflush(stdin);
            }
            for(i=0;i<e;i++)
            {
                num1=rand()%10+1;
                num2=rand()%10+1;
                num3=rand()%10+1;
                num4=rand()%10+1;
                c=rand()%4+1;
                if(num1>num2)
                {
                    num5=num1;
                    num1=num2;
                    num2=num5;
                }
                printf("\n(%d/%d)",num1,num2);
                 switch(c)
                {
                case 1:printf("+");
                    op='+';
                    break;
                case 2:printf("-");
                        op='-';
                        break;
                case 3:printf("*");
                        op='*';
                        break;
                case 4:printf("/");
                        op='/';
                        break;
                }
                if(num3>num4)
                {
                    num5=num3;
                    num3=num4;
                    num4=num5;
                }
                printf("(%d/%d)=",num3,num4);
               /* op='+';
                num6=num1*num4+num2*num3;
                num7=num2*num4;
                num8=num6;
                while(num8>1)
                {
                    if(num6%num8==0 && num7%num8==0)
                    {
                        num6=num6/num8;
                        num7=num7/num8;
                    }
                    num8--;
                }
                printf("\n(%d/%d)%c(%d/%d)=",num1,num2,op,num3,num4);*/


                    
                while(!(scanf("%d%c%d",&g,&op1,&h)))
                {
                    printf("\n 非法输入，请重新输入：");
                    fflush(stdin);
                }
                switch(op)
                {   
                case '+':   
                    num6=num1*num4+num2*num3;
                	num7=num2*num4;
                	num8=GCD(num6,num7);
                	num6/=num8;
                	num7/=num8;
                    break;
                case '-':   
                    num6=num1*num4-num2*num3;
                	num7=num2*num4;
                	num8=GCD(num6,num7);
                	num6/=num8;
                	num7/=num8;
                    break;
                case '*':   
                    num6=num1*num3;
                	num7=num2*num4;
                	num8=GCD(num6,num7);
                	num6/=num8;
                	num7/=num8;
                    break;
                case '/':  
                    num6=num1*num4;
                	num7=num2*num3;
                	num8=GCD(num6,num7);
                	num6/=num8;
                	num7/=num8;
                    break; 
                }
                if((g==num6)&&(h==num7))
                {
                    printf("\n恭喜你，答对了！\n");
                    j++;
                }
                else
                {
                    printf("\n很遗憾，答错了...正确答案是");
                    printf("(%d/%d)\n",num6,num7);
                }
            }
            	 rate=1.0*j/e*100;
                 printf("用户总共答对了%d道题目！ 正确率为%.2f%%\n",j,rate);
        }
        break;
    
        
        }
    }while(n!=-1);
    
}

