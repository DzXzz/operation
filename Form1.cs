﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        int a, b, c, d, j = 0, k, n, g, h,  num1, num2, num3, num4;
        int getanswer;                     //e为记录用户的题量，f为用户的答题答案 
        double rate;
        int num5, num6, num7, num8;     //临时变量
        Stopwatch watch = new Stopwatch();
        public int GCD(int k, int l)    //求最大公约数
        {
            int temp;                   /*定义整型变量*/
            if (k < l)                  /*通过比较求出两个数中的最大值和最小值*/
            {
                temp = k;
                k = l;
                l = temp;
            }                           /*设置中间变量进行两数交换*/
            while (l != 0)              /*通过循环求两数的余数，直到余数为0*/
            {
                temp = k % l;
                k = l;                  /*变量数值交换*/
                l = temp;
            }
            return k;
        }
        public int GetRows(string FilePath)
        {
            using (StreamReader read = new StreamReader(FilePath, Encoding.Default))
            {
                return read.ReadToEnd().Split('\n').Length;
            }
        }
        public void Write(string path,string line)
        {
           
            StreamWriter sw = File.AppendText(path);
            //开始写入
            sw.Write(line);
            //清空缓冲区
            sw.Flush();
            //关闭流
            sw.Close();
            
        }
        public Form1()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            textBox2.Clear();
            dataGridView1.Rows.Clear(); 
            dataGridView2.Rows.Clear();
            n = int.Parse(textBox1.Text);//用户控制输入试题数目 
            textBox2.Text += "尊敬的用户您好，您的请求已经得到确认" + "\r\n";
            textBox2.Text += "您将打印 " + n  + " 道题目" + "\r\n";
            System.Random number = new Random(System.DateTime.Now.Millisecond);
            watch.Start();
            switch (comboBox1.Text)
            {
                case "整数":
                    {
                        
                        for(int i = 0; i < n; i++)
                        {
                            num1=number .Next(0,100);
                            num2=number .Next(0,100);
                            k = number.Next(0,4);
                            switch(k)
                            {
                                case 0: dataGridView1.Rows.Add(i + 1, num1, "+", num2, "="); break;
                                case 1: dataGridView1.Rows.Add(i + 1, num1, "-", num2, "="); break;
                                case 2: dataGridView1.Rows.Add(i + 1, num1, "*", num2, "="); break;
                                case 3: { if (num2 != 0) dataGridView1.Rows.Add(i + 1, num1, "/", num2, "="); else i = i - 1; break; }
                            }

                        }
                    }break;
                case "分数":
                    {
                        for (int i = 0; i < n; i++)
                        {
                            num1 = number.Next(0, 100);
                            num2 = number.Next(0, 100);
                            num3 = number.Next(0, 100);
                            num4 = number.Next(0, 100);
                            k = number.Next(0, 4);
                            if (num3 > num4)
                            {
                                num5 = num3;
                                num3 = num4;
                                num4 = num5;
                            }
                            if (num1 != num2 && num3 != num4)
                            {
                                switch (k)
                                { 
                                    case 0: dataGridView2.Rows.Add(i + 1, num1, "/", num2, "+", num3, "/", num4, "=", null, "/"); break;
                                    case 1: dataGridView2.Rows.Add(i + 1, num1, "/", num2, "-", num3, "/", num4, "=", null, "/"); break;
                                    case 2: dataGridView2.Rows.Add(i + 1, num1, "/", num2, "*", num3, "/", num4, "=", null, "/"); break;
                                    case 3: { if (num2 != 0 && num3 != 0 && num4 != 0) dataGridView2.Rows.Add(i + 1, num1, "/", num2, "/", num3, "/", num4, "=", null, "/"); else i = i - 1; break; }
                                }
                            }
                        } break;
                        

                    }
                 

            }

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            watch.Stop();
            int count = 0;
            string time = watch.Elapsed.ToString();
            string path = "D:\\TestFile.txt";
            switch (comboBox1.Text)
            {
                   
                case "整数":
                    {
                        for (int i = 0; i < n; i++) 
                        {
                            if (this.dataGridView1.Rows[i].Cells[1].Value.ToString() != null)
                            {
                                a = int.Parse(this.dataGridView1.Rows[i].Cells[1].Value.ToString());
                            }
                            if (this.dataGridView1.Rows[i].Cells[3].Value.ToString() != null)
                            {
                                b = int.Parse(this.dataGridView1.Rows[i].Cells[3].Value.ToString());
                            }
                            switch (this.dataGridView1.Rows[i].Cells[2].Value.ToString())
                            {
                                case "+":
                                    {
                                        getanswer = a + b;
                                    } break;
                                case "-":
                                    {
                                        getanswer = a - b;
                                    } break;
                                case "*":
                                    {
                                        getanswer = a * b;
                                    } break;
                                case "/":
                                    {
                                        getanswer = a / b;
                                    } break;
                            }
                            if (this.dataGridView1.Rows[i].Cells[5].Value != null)
                            {
                                j = int.Parse(this.dataGridView1.Rows[i].Cells[5].Value.ToString());
                                if (getanswer == j)
                                {
                                    this.dataGridView1.Rows[i].Cells[6].Value = "正确";
                                }
                                else
                                {
                                    this.dataGridView1.Rows[i].Cells[6].Value = "错误";
                                    count = count + 1;
                                }

                            }
                            else
                            {
                                this.dataGridView1.Rows[i].Cells[6].Value = "错误";
                                count = count + 1;
                            }
                            string celldata = this.dataGridView1.Rows[i].Cells[6].Value.ToString();
                            if (celldata == "错误") 
                            {
                                string wrong = "";
                                for (int m = 1; m < 5; m++) 
                                {
                                    wrong  =wrong + this.dataGridView1.Rows[i].Cells[m].Value.ToString();
                                }
                                wrong = wrong + getanswer + "\r\n";
                                Write(path, wrong);
                            }
                        }
                    }break;
                case "分数":
                    {
                        for (int i = 0; i < n; i++)
                        {
                            if (this.dataGridView2.Rows[i].Cells[1].Value.ToString() != null)
                            {
                                a = int.Parse(this.dataGridView2.Rows[i].Cells[1].Value.ToString());
                            }
                            if (this.dataGridView2.Rows[i].Cells[3].Value.ToString() != null)
                            {
                                b = int.Parse(this.dataGridView2.Rows[i].Cells[3].Value.ToString());
                            }
                            if (this.dataGridView2.Rows[i].Cells[5].Value.ToString() != null)
                            {
                                c = int.Parse(this.dataGridView2.Rows[i].Cells[5].Value.ToString());
                            }
                            if (this.dataGridView2.Rows[i].Cells[7].Value.ToString() != null)
                            {
                                d = int.Parse(this.dataGridView2.Rows[i].Cells[7].Value.ToString());
                            }
                            switch (this.dataGridView2.Rows[i].Cells[2].Value.ToString())
                            {
                                case "+":
                                    {
                                        num6 = a * d + b * c;
                                        num7 = b * d;
                                        num8 = GCD(num6, num7);
                                        num6 /= num8;
                                        num7 /= num8;
                                    }break;
                                case "-":
                                    {
                                        num6 = a * d - b * c;
                                        num7 = b * d;
                                        num8 = GCD(num6, num7);
                                        num6 /= num8;
                                        num7 /= num8;
                                    } break;
                                case "*":
                                    {
                                        num6 = a * c;
                                        num7 = b * d;
                                        num8 = GCD(num6, num7);
                                        num6 /= num8;
                                        num7 /= num8;
                                    } break;
                                case "/":
                                    {
                                        num6 = a * d;
                                        num7 = b * c;
                                        num8 = GCD(num6, num7);
                                        num6 /= num8;
                                        num7 /= num8;
                                    } break;
                            }
                            if (this.dataGridView2.Rows[i].Cells[9].Value != null && this.dataGridView2.Rows[i].Cells[11].Value != null)
                            {
                                g = int.Parse(this.dataGridView2.Rows[i].Cells[9].Value.ToString());
                                h = int.Parse(this.dataGridView2.Rows[i].Cells[11].Value.ToString());
                                if ((g == num6) && (h == num7))
                                {
                                    this.dataGridView2.Rows[i].Cells[12].Value = "正确";
                                }
                                else
                                {
                                    this.dataGridView2.Rows[i].Cells[12].Value = "错误";
                                    count = count + 1;
                                }
                            }
                            else
                            {
                                this.dataGridView2.Rows[i].Cells[12].Value = "错误";
                                count = count + 1;
                            }
                            string celldata = this.dataGridView2.Rows[i].Cells[12].Value.ToString();
                            if (celldata == "错误")
                            {
                                string wrong = "";
                                for (int m = 1; m < 9; m++)
                                {
                                    wrong = wrong + this.dataGridView2.Rows[i].Cells[m].Value.ToString();
                                }
                                wrong = wrong + num6 + "/" + num7 + "\r\n";
                                Write(path, wrong);
                            }

                        }
                        
                    }break;
            }
            int totalcount;
            totalcount = GetRows(path);
            rate = Convert.ToDouble(n-count) / Convert.ToDouble(n);
            string result = rate.ToString("0.00%");
            textBox2.Text += "正确数目：" + (n - count) + "\r\n";
            textBox2.Text += "错误数目：" +count  + "\r\n";
            textBox2.Text += "正确率：" + result + "\r\n";
            textBox2.Text += "总计错误数目：" + totalcount + "\r\n";
            textBox2.Text += "用时：" + time + "\r\n";
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

    }
}
